<?php
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_jinosurvey_mod_form extends moodleform_mod {

    /** @var array options to be used with date_time_selector fields in the quiz. */
    public static $datefieldoptions = array('optional' => true);

    function definition() {

        global $CFG, $DB;

        $mform =& $this->_form;

        $strrequired = get_string('required');

        //-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name','jinosurvey'), array('size'=>'64','placeholder' => get_string('name','jinosurvey')));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');

        $this->standard_intro_elements(get_string('introformat', 'jinosurvey'));

        // Open and close dates.
        $mform->addElement('date_time_selector', 'timeopen', get_string('timeopen', 'jinosurvey'),
                self::$datefieldoptions);

        $mform->addElement('date_time_selector', 'timeclose', get_string('timeclose', 'jinosurvey'),
                self::$datefieldoptions);
                
        $this->standard_coursemodule_elements();

        //-------------------------------------------------------------------------------
        // buttons
        $this->add_action_buttons();
    }
}

