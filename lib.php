<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   mod_jinosurvey
 * @copyright 2019, Theodore <hoquangthaiholy@gmail.com>
 * @license   JinoTech 2019
 */

 // STANDARD FUNCTIONS ////////////////////////////////////////////////////////
/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @global object
 * @param object $survey
 * @return int|bool
 */
function jinosurvey_add_instance($survey) {
    global $DB;

    $survey->timecreated  = time();
    $survey->timemodified = $survey->timecreated;

    $id = $DB->insert_record('jinosurvey', $survey);

    $completiontimeexpected = !empty($survey->completionexpected) ? $survey->completionexpected : null;
    \core_completion\api::update_completion_date_event($survey->coursemodule, 'jinosurvey', $id, $completiontimeexpected);
    return $id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @global object
 * @param object $survey
 * @return bool
 */
function jinosurvey_update_instance($survey) {
    global $DB;

    $survey->id           = $survey->instance;
    $survey->timemodified = time();

    $completiontimeexpected = !empty($survey->completionexpected) ? $survey->completionexpected : null;
    \core_completion\api::update_completion_date_event($survey->coursemodule, 'jinosurvey', $survey->id, $completiontimeexpected);

    return $DB->update_record('jinosurvey', $survey);
}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @global object
 * @param int $id
 * @return bool
 */
function jinosurvey_delete_instance($id) {
    global $DB;

    if (! $survey = $DB->get_record('jinosurvey', array('id'=>$id))) {
        return false;
    }

    $cm = get_coursemodule_from_instance('jinosurvey', $id);
    \core_completion\api::update_completion_date_event($cm->id, 'jinosurvey', $id, null);

    $result = true;

    if (! $DB->delete_records('jinosurvey_answers', array('survey'=>$survey->id))) {
        $result = false;
    }

    if (! $DB->delete_records('jinosurvey', array('id'=>$survey->id))) {
        $result = false;
    }

    return $result;
}