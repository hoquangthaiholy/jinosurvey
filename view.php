<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   mod_jinosurvey
 * @copyright 2018, Theodore <hoquangthaiholy@gmail.com>
 * @license   JinoTech 2018
 */

require_once("../../config.php");
require_once("lib.php");

$id = required_param('id', PARAM_INT);    // Course Module ID.

if (! $cm = get_coursemodule_from_id('jinosurvey', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}

$PAGE->set_url('/mod/jinosurvey/view.php', array('id' => $id));
require_login($course, false, $cm);
$context = context_module::instance($cm->id);

require_capability('mod/jinosurvey:participate', $context);

if (! $survey = $DB->get_record("jinosurvey", array("id" => $cm->instance))) {
    print_error('invalidsurveyid', 'jinosurvey');
}

$strsurvey = get_string("modulename", "jinosurvey");
$PAGE->set_title($survey->name);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
echo $OUTPUT->heading($survey->name);
echo $OUTPUT->box(format_module_intro('survey', $survey, $cm->id), 'generalbox boxaligncenter bowidthnormal', 'intro');

if (!is_enrolled($context)) {
    echo $OUTPUT->notification(get_string("guestsnotallowed", "jinosurvey"));
}

if (!has_capability('mod/jinosurvey:managesurvey', $context) && $survey->uselink==1) {
?>
    <div class="text-center">
        <a class="btn btn-primary btn-lg" href="<?php echo $survey->externallink; ?>" target='_blank'><?php echo get_string("externallink_button", "jinosurvey"); ?></a>
    </div>
<?php
} else {
?>
    <div
        id="app"
        route="api.php"
        lang="<?php echo current_language(); ?>"
        activityid=<?php echo $id; ?>
        sesskey="<?php echo $USER->sesskey; ?>"
        surveyid="<?php echo $survey->id; ?>"
        userid="<?php echo $USER->id; ?>"
        <?php if (has_capability('mod/jinosurvey:managesurvey', $context)){ echo 'is-admin="true"'; } ?>
    >
    </div>
    <script src="//localhost:8080/app.js"></script>
    <!-- <script src="com/dist/js/app.f552f182.js"></script>
    <script src="com/dist/js/chunk-vendors.7b398ad4.js"></script>
    <link rel="stylesheet" href="com/dist/css/app.ed3f44a2.css"> -->
<?php

}
echo $OUTPUT->footer();


