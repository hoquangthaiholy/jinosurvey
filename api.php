<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   mod_jinosurvey
 * @copyright 2018, Theodore <hoquangthaiholy@gmail.com>
 * @license   JinoTech 2018
 */

if (!defined('AJAX_SCRIPT')) {
    define('AJAX_SCRIPT', true);
}

require_once("../../config.php");
require_once("lib.php");

// Initialise ALL the incoming parameters here, up front.
$surveyid   = required_param('surveyid', PARAM_INT);
$action     = required_param('action', PARAM_RAW);
$type       = optional_param('type','',PARAM_ALPHAEXT);
$title      = optional_param('title','',PARAM_TEXT);
$content    = optional_param('content','',PARAM_TEXT);
$params     = optional_param('params','',PARAM_TEXT);
$userid     = optional_param('userid',0,PARAM_INT);
$answers    = optional_param('answers','',PARAM_TEXT);
$other      = optional_param('other','',PARAM_TEXT);
$description= optional_param('description','',PARAM_CLEANHTML);
$questionid = optional_param('questionid','',PARAM_INT);
$questions  = optional_param('questions','',PARAM_ALPHA);
$required   = optional_param('required','',PARAM_BOOL);
$uselink    = optional_param('uselink','',PARAM_BOOL);

$surveyid_import      = optional_param('surveyid_import','',PARAM_INT);
$externallink         = optional_param('externallink','',PARAM_TEXT);
$allowuserinputitem   = optional_param('allowuserinputitem','',PARAM_BOOL);

$PAGE->set_url('/mod/jinosurvey/api.php',
        array('surveyid' => $surveyid));

require_sesskey();
$survey = $DB->get_record('jinosurvey', array('id' => $surveyid), '*', MUST_EXIST);
$cm = get_coursemodule_from_instance('jinosurvey', $survey->id, $survey->course);
$course = $DB->get_record('course', array('id' => $survey->course), '*', MUST_EXIST);
require_login($course, false, $cm);

$context = context_module::instance($cm->id);

echo $OUTPUT->header(); // Send headers.

// All these AJAX actions should be logically atomic.
$transaction = $DB->start_delegated_transaction();

$result = null;
$requestmethod = $_SERVER['REQUEST_METHOD'];
global $DB;

switch($requestmethod) {
    case 'POST':
    case 'GET': // For debugging.
        switch($action){
            case 'get_survey':
                require_capability('mod/jinosurvey:participate', $context);
                $questionids = explode(',', $survey->questions);
                $rows = $DB->get_records_list("jinosurvey_questions", "id", $questionids);
                $result = $survey;
                $result->questions = [];
                foreach ($rows as $row)
                    $result->questions[] = $row;

                $result->user_done_count = $DB->get_records_sql('SELECT DISTINCT COUNT(userid) AS user_done_count FROM {jinosurvey_answers} WHERE survey = ?', array($survey->id))[1]->user_done_count;
                break;

            case 'get_survey_answers':
                require_capability('mod/jinosurvey:participate', $context);
                $questionids = explode(',', $survey->questions);
                $rows = $DB->get_records_list("jinosurvey_questions", "id", $questionids);
                $result = $survey;
                $result->questions = [];
                foreach ($rows as $row) {
                    $answer = $DB->get_record('jinosurvey_answers', array(
                        'userid' => $userid,
                        'survey' => $surveyid,
                        'question' => $row->id
                    ));
                    if ($answer){
                        $row->answer = $answer->answer;
                        $row->other = $answer->other;
                    }
                    $result->questions[] = $row;
                }

                $result->finished = $DB->record_exists("jinosurvey_answers", array("survey"=>$surveyid, "userid"=>$userid));
            break;

            case 'import_question':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $question = $DB->get_record('jinosurvey_questions', array('id' => $questionid));
                $questionids = explode(',', $survey->questions);
                
                $questionid = $DB->insert_record('jinosurvey_questions', $question);
                $questionids[] = $questionid;
                $DB->update_record('jinosurvey', $survey);

                $result = $DB->get_record('jinosurvey_questions', array('id' => $questionid));

                break;

                case 'import_survey':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $import = $DB->get_record('jinosurvey', array('id' => $surveyid_import));
                $import_questionids = explode(',', $import->questions);
                $questionids = explode(',', $survey->questions);
                $rows = $DB->get_records_list("jinosurvey_questions", "id", $import_questionids);
                $result = [];
                foreach ($rows as $row){
                    unset($row->id);
                    $questionid = $DB->insert_record('jinosurvey_questions', $row);
                    $questionids[] = $questionid;
                    $result[] = $DB->get_record('jinosurvey_questions', array('id' => $questionid));
                }
                
                $DB->update_record('jinosurvey',array(
                    'id' => $survey->id,
                    'questions' => implode(',',array_filter($questionids))
                ));
                
                break;
            
            case 'get_surveys':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $rows = $DB->get_records_sql("SELECT * FROM {jinosurvey} WHERE id != ? AND uselink = 0", array($survey->id));
                $result->surveys = [];
                foreach ($rows as $row)
                    $result->surveys[] = $row;
                break;

            case 'get_questions_import':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $rows = $DB->get_records_sql("SELECT * FROM {jinosurvey_questions}");
                $result->questions = [];
                foreach ($rows as $row)
                    $result->questions[] = $row;
                break;

            case 'get_questions':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $rows = $DB->get_records_list("jinosurvey", "id", $questionids);
                $result->data = [];
                foreach ($rows as $row) {
                    $result->data[] = $row;
                }
                break;

            case 'add_new_question':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $question->type = $type;
                $question->title = $title;
                $question->description = $description;

                $question->timecreated  = time();
                $question->timemodified = $survey->timecreated;

                $questionid = $DB->insert_record('jinosurvey_questions', $question);
                $questionids = explode(',', $survey->questions);
                $questionids[] = $questionid;

                $survey->questions = implode(',',array_filter($questionids));
                $DB->update_record('jinosurvey', $survey);
                
                $result = $DB->get_record('jinosurvey_questions', array('id' => $questionid));
                break;
            
            case 'survey_setlink':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $result = $DB->update_record('jinosurvey',array(
                    'id' => $surveyid,
                    'uselink' => $uselink
                ));
                break;
            
            case 'survey_changelink':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $result = $DB->update_record('jinosurvey',array(
                    'id' => $surveyid,
                    'externallink' => $externallink
                ));
                break;

            case 'survey_reorder':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $result = $DB->update_record('jinosurvey',array(
                    'id' => $surveyid,
                    'questions' => $questions
                ));
                break;

            case 'delete_question':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $questionids = explode(',', $survey->questions);
                if (($key = array_search($questionid, $questionids)) !== false) {
                    unset($questionids[$key]);
                }
                $survey->questions = implode(',',array_filter($questionids));
                $DB->update_record('jinosurvey', $survey);

                $result = array('data' => $DB->delete_records('jinosurvey_questions',array('id' => $questionid)));
                break;

            case 'update_question':
                require_capability('mod/jinosurvey:managesurvey', $context);
                $question->id = $questionid;
                $question->title = $title;
                $question->description = $description;
                $question->required = $required;
                $question->allowuserinputitem = $allowuserinputitem;
                $question->content = $content;
                $question->params = $params;

                $question->timemodified = time();
                
                $result = $DB->update_record('jinosurvey_questions',$question);
                break;

            case 'submit_answers':
                require_capability('mod/jinosurvey:participate', $context);
                $answers = json_decode($answers);

                foreach ($answers as $key => $value) {
                    $answer->userid = $userid;
                    $answer->survey = $surveyid;
                    $answer->question = $key;
                    $answer->time = time();

                    if (is_object($value[0])){
                        $answer->answer = implode(';',array_column($value,'value'));
                    } else if (is_array($value)){
                        $answer->answer = implode(';',$value);
                    } else {
                        $answer->answer = $value;
                    }

                    $DB->insert_record('jinosurvey_answers', $answer);
                }

                $result = true;
                break;
        }
        break;
}

$transaction->allow_commit();

echo json_encode($result);
