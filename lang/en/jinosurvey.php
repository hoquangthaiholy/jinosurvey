<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   mod_jinosurvey
 * @copyright 2019, Theodore <hoquangthaiholy@gmail.com>
 * @license   JinoTech 2019
 */

// Module information
$string['pluginname'] = 'Jino Survey';
$string['modulename'] = 'Jino Survey';
$string['modulename_help'] = '<img class="img-fluid" src="http://www.jinotech.com/wp-content/uploads/2018/09/logo.png">

The Jino Survey activity module provides a number of verified survey instruments that have been found useful in assessing and stimulating learning in online environments. A teacher can use these to gather data from their students that will help them learn about their class and reflect on their own teaching.

Note that these survey tools are pre-populated with questions. Teachers who wish to create their own survey should use the feedback activity module.';
$string['modulename_link'] = 'mod/jinosurvey/view';
$string['modulenameplural'] = 'Jino Surveys';
$string['pluginadministration'] = 'Jino Survey';

// Module Form
$string['name'] = 'Title';
$string['introformat'] = 'Content';
$string['timeopen'] = 'Time open';
$string['timeclose'] = 'Time close';

// View
$string['invalidsurveyid'] = 'Survey ID was incorrect';
$string['guestsnotallowed'] = 'Only enrolled users are able to submit surveys';
$string['externallink_button'] = 'Go to survey';
