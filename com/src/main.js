import Vue from 'vue'
import App from './App.vue'
import PrettyCheckbox from 'pretty-checkbox-vue'
import Notifications from 'vue-notification'
import VueSwal from 'vue-swal'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo)
Vue.use(VueSwal)
Vue.use(PrettyCheckbox)
Vue.use(Notifications)

Vue.filter('striphtml', function (value) {
  var div = document.createElement("div");
  div.innerHTML = value;
  var text = div.textContent || div.innerText || "";
  return text;
});

import axios from 'axios'

Vue.config.productionTip = false

new Vue({
  props: {
    lang: String,
    strings: Object,
    sesskey: String,
    route: String,
    surveyid: Number,
    isAdmin: Boolean,
    editable: Boolean,
    userid: Number,
    activityid: Number
  },
  beforeMount() {
    this.sesskey = this.$el.getAttribute('sesskey');
    this.lang = this.$el.getAttribute('lang');
    this.route = this.$el.getAttribute('route');
    this.surveyid = this.$el.getAttribute('surveyid');
    this.userid = this.$el.getAttribute('userid');
    this.activityid = this.$el.getAttribute('activityid');
    this.isAdmin = this.$el.getAttribute('is-admin')=='true';
    this.getLang(this.lang,this.getLang('en'));
  },
  methods: {
    getLang(code,error){
      axios('com/public/lang/'+code+'.json')
      .then(res => this.strings = res.data)
      .catch(error)
    },
    str(name){
      return this.strings && this.strings[name] || '';
    },
    random_str(length){
      Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0,length)
    }
  },
  render: h => h(App),
}).$mount('#app')
