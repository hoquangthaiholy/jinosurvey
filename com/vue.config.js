module.exports = {
  devServer: {
    host: "localhost",
    port: 8080,
    https: false,
    hotOnly: false
  }
};
